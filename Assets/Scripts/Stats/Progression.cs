﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Stats
{
    [CreateAssetMenu(fileName = "Progression", menuName = "Stats/Make New Progression", order = 0)]
    public class Progression : ScriptableObject
    {
        [SerializeField] private ProgressionCharacterClass[] _characterClasses = null;

        Dictionary<CharacterClass, Dictionary<Stat, float[]>> _lookupTable = null;

        public float GetStat(Stat stat, CharacterClass characterClass, int level)
        {
            BuildLookup();

            float[] levels = _lookupTable[characterClass][stat];

            if (levels.Length < level) return 0;

            return levels[level - 1];
        }

        public int GetLevelsAmount(Stat stat, CharacterClass characterClass)
        {
            BuildLookup();

            return _lookupTable[characterClass][stat].Length;
        }

        public void BuildLookup()
        {
            if (_lookupTable != null) return;

            _lookupTable = new Dictionary<CharacterClass, Dictionary<Stat, float[]>>();

            foreach (ProgressionCharacterClass progressionClass in _characterClasses)
            {
                CharacterClass characterClass = progressionClass.characterClass;

                _lookupTable.Add(characterClass, new Dictionary<Stat, float[]>());

                foreach (ProgressionStat progressionStat in progressionClass.stats)
                {
                    Stat stat = progressionStat.stat;

                    _lookupTable[characterClass].Add(stat, progressionStat.levels);
                }
            }
        }

        [System.Serializable]
        class ProgressionCharacterClass
        {
            public CharacterClass characterClass;
            public ProgressionStat[] stats;
        }

        [System.Serializable]
        class ProgressionStat
        {
            public Stat stat;
            public float[] levels;
        }
    }
}