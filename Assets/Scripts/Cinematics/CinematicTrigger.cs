﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

namespace RPG.Cinematics
{
    public class CinematicTrigger : MonoBehaviour
    {
        private bool _isTriggered = false;

        private void OnTriggerEnter(Collider other)
        {
            if (other.tag == "Player" && !_isTriggered)
            {
                GetComponent<PlayableDirector>().Play();
                _isTriggered = true;
            }
        }
    }
}
