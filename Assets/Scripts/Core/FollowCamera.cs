﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Core
{
    public class FollowCamera : MonoBehaviour
    {
        [SerializeField]
        private Transform m_target;

        void LateUpdate()
        {
            transform.position = m_target.position;
        }
    }
}

