﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.AI;
using RPG.Control;

namespace RPG.SceneManagement
{
    public class Portal : MonoBehaviour
    {
        enum DestinationIdentifier
        {
            A, B, C, D
        }

        [SerializeField] int _sceneId;
        [SerializeField] Transform _spawnPoint;
        [SerializeField] float _fadeTime = 2f;
        [SerializeField] DestinationIdentifier _destination;

        private void OnTriggerEnter(Collider other)
        {
            if (other.tag == "Player")
            {
                StartCoroutine(Transition());
            }
        }

        private IEnumerator Transition()
        {  
            if (_sceneId < 0)
            {
                Debug.Log("No Scene ID has been set");
                yield break;
            }

            DontDestroyOnLoad(gameObject);

            Fader fader = FindObjectOfType<Fader>();
            SavingWrapper savingWrapper = FindObjectOfType<SavingWrapper>();
            PlayerController prevController = GameObject.FindWithTag("Player").GetComponent<PlayerController>();
            prevController.enabled = false;

            yield return fader.FadeOut(_fadeTime);

            savingWrapper.Save();

            yield return SceneManager.LoadSceneAsync(_sceneId);
            PlayerController newController = GameObject.FindWithTag("Player").GetComponent<PlayerController>();
            newController.enabled = false;

            savingWrapper.Load();

            Portal otherPortal = GetOtherPortal();
            UpdatePlayer(otherPortal);

            savingWrapper.Save();

            yield return fader.FadeIn(_fadeTime);

            newController.enabled = true;
            Destroy(gameObject);
        }

        private Portal GetOtherPortal()
        {
            foreach (Portal portal in FindObjectsOfType<Portal>())
            {
                if (portal == this) continue;
                if (portal._destination == _destination) return portal;
            }

            return null;
        }

        private void UpdatePlayer(Portal otherPortal)
        {
            GameObject player = GameObject.FindWithTag("Player");
            player.GetComponent<NavMeshAgent>().Warp(otherPortal._spawnPoint.position);
            player.transform.rotation = otherPortal._spawnPoint.rotation;
        }
    }
}
