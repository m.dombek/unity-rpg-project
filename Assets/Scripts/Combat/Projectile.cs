﻿using RPG.Attributes;
using UnityEngine;
using UnityEngine.Events;

namespace RPG.Combat
{
    public class Projectile : MonoBehaviour
    {
        [SerializeField] private float _speed = 5f;
        [SerializeField] private bool _isHoming = false;
        [SerializeField] private GameObject _hitEffect = null;
        [SerializeField] private float _maxLifetime = 5f;
        [SerializeField] private float _lifeAfterImpact = 2;
        [SerializeField] private GameObject[] _destroyOnHit = null;
        [SerializeField] UnityEvent onHit;

        private GameObject _instigator = null;
        private float _damage;
        private Health _target;

        private void Start()
        {
            transform.LookAt(GetAimLocation());
        }

        // Update is called once per frame
        void Update()
        {
            if (_target == null) return;
            if (_isHoming && _target.IsAlive()) transform.LookAt(GetAimLocation());

            transform.Translate(Vector3.forward * Time.deltaTime * _speed);
        }

        public void SetTarget(Health target, GameObject instigator, float damage)
        {
            _target = target;
            _damage = damage;
            _instigator = instigator;

            Destroy(gameObject, _maxLifetime);
        }

        private Vector3 GetAimLocation()
        {
            CapsuleCollider targetCapsule = _target.GetComponent<CapsuleCollider>();

            if (targetCapsule == null) return _target.transform.position;

            return _target.transform.position + Vector3.up * targetCapsule.height / 2;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.GetComponent<Health>() != _target) return;
            if (!_target.IsAlive()) return;
            _target.TakeDamage(_instigator, _damage);
            _speed = 0;

            if (_hitEffect != null)
            {
                GameObject hitEffect = Instantiate(_hitEffect, GetAimLocation(), transform.rotation);
            }

            onHit.Invoke();

            foreach (GameObject toDestroy in _destroyOnHit)
            {
                Destroy(toDestroy);
            }

            Destroy(this.gameObject, _lifeAfterImpact);
        }
    }
}
