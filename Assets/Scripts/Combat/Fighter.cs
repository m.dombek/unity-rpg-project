﻿using RPG.Core;
using RPG.Movement;
using UnityEngine;
using RPG.Saving;
using RPG.Attributes;
using RPG.Stats;
using System.Collections.Generic;

namespace RPG.Combat
{
    public class Fighter : MonoBehaviour, IAction, ISaveable, IModifierProvider
    {
        [SerializeField] private float _timeBetweenAttacks = 1.0f;
        [SerializeField] Transform _rightHand = null;
        [SerializeField] Transform _leftHand = null;
        [SerializeField] WeaponConfig _defaultWeaponConfig = null;

        private Health _target;
        private float timeSinceLastAttack = Mathf.Infinity;
        private WeaponConfig _currentWeaponConfig = null;
        private Weapon _currentWeapon;

        private void Start()
        {
            if (_currentWeaponConfig == null)
            {
                EquipWeapon(_defaultWeaponConfig);
            }
        }

        private void Update()
        {
            timeSinceLastAttack += Time.deltaTime;

            if (_target == null) return; // If there's no Target, return.
            if (!_target.IsAlive()) return;

            // If there's a Target and the Player isn't in range yet, move towards the Target
            if (!GetIsInRange())
            {
                GetComponent<Mover>().StartMoveAction(_target.transform.position);
            }
            // Otherwise, stop moving once in range
            else
            {
                GetComponent<Mover>().Cancel();
                AttackBehaviour();
            }
        }

        public void EquipWeapon(WeaponConfig weapon)
        {
            _currentWeaponConfig = weapon;
            Animator animator = GetComponent<Animator>();
            weapon.Spawn(_rightHand, _leftHand, animator);
        }

        public Health GetTarget()
        {
            return _target;
        }

        private void AttackBehaviour()
        {
            transform.LookAt(_target.transform);
            if (timeSinceLastAttack > _timeBetweenAttacks)
            {
                // This will trigger the Hit() event
                TriggerAttackAnimation();
                timeSinceLastAttack = 0;
            }
        }

        private void TriggerAttackAnimation()
        {
            GetComponent<Animator>().ResetTrigger("Attack");
            GetComponent<Animator>().SetTrigger("Attack");
        }

        public bool CanAttack(GameObject target)
        {
            if (target == null) return false;

            Health targetToTest = target.GetComponent<Health>();
            return targetToTest != null && targetToTest.IsAlive();
        }

        // Animation event
        private void Hit()
        {
            if (_target == null) return;

            float damage = GetComponent<BaseStats>().GetStat(Stat.Damage);

            if (_currentWeaponConfig.HasProjectile())
            {
                _currentWeaponConfig.LaunchProjectile(_rightHand, _leftHand, _target, gameObject, damage);
            }
            else
            {
                _target.TakeDamage(gameObject, damage);
            } 
            
        }

        // Animation Event
        private void Shoot()
        {
            Hit();
        }

        private bool GetIsInRange()
        {
            return Vector3.Distance(transform.position, _target.transform.position) < _currentWeaponConfig.GetRange();
        }

        public void Attack(GameObject target)
        {
            GetComponent<ActionScheduler>().StartAction(this);            
            _target = target.GetComponent<Health>();
        }

        public void Cancel()
        {
            TriggerStopAttackAnimation();
            GetComponent<Mover>().Cancel();
            _target = null;
        }

        private void TriggerStopAttackAnimation()
        {
            GetComponent<Animator>().ResetTrigger("Attack");
            GetComponent<Animator>().SetTrigger("StopAttack");
        }

        public IEnumerable<float> GetAdditiveModifiers(Stat stat)
        {
            if (stat == Stat.Damage)
            {
                yield return _currentWeaponConfig.GetDamage();
            }
        }

        public IEnumerable<float> GetPerecntageModifiers(Stat stat)
        {            
            if (stat == Stat.Damage)
            {
                yield return _currentWeaponConfig.GetPercentageBonus();
            }
        }

        public object CaptureState()
        {
            if (_currentWeaponConfig == null) return null;

            return _currentWeaponConfig.name;
        }

        public void RestoreState(object state)
        {
            string weaponName = (string)state;
            WeaponConfig weapon = UnityEngine.Resources.Load<WeaponConfig>(weaponName);
            EquipWeapon(weapon);
        }
    }
}