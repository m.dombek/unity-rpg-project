﻿using RPG.Attributes;
using UnityEngine;
using UnityEngine.UI;

namespace RPG.Combat
{
    public class EnemyHealthDisplay : MonoBehaviour
    {
        Health target;

        private void Update()
        {
            target = GameObject.FindWithTag("Player").GetComponent<Fighter>().GetTarget();

            if (target == null)
            {
                GetComponent<Text>().text = "N/A";
                return;
            }
            
            GetComponent<Text>().text = string.Format("{0:0}/{1:0}", target.GetHealthPoints(), target.GetMaxHealthPoints());
        }
    }
}
