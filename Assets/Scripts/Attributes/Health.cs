﻿using RPG.Core;
using RPG.Saving;
using RPG.Stats;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace RPG.Attributes
{    
    public class Health : MonoBehaviour, ISaveable
    {
        [SerializeField] private float _healthPoints = -1f;
        [SerializeField] TakeDamageEvent takeDamage;
        [SerializeField] UnityEvent onDie;

        [System.Serializable]
        public class TakeDamageEvent : UnityEvent<float>
        {
        }

        private bool _isAlive = true;

        private void Start()
        {  
            if (_healthPoints < 0)
            {
                _healthPoints = GetComponent<BaseStats>().GetStat(Stat.Health);
            }
        }

        private void OnEnable()
        {
            GetComponent<BaseStats>().onLevelUp += RestoreHealth;
        }

        private void OnDisable()
        {
            GetComponent<BaseStats>().onLevelUp -= RestoreHealth;
        }

        public float GetHealthPoints()
        {
            return _healthPoints;
        }

        public float GetMaxHealthPoints()
        {
            return GetComponent<BaseStats>().GetStat(Stat.Health);
        }

        private void RestoreHealth()
        {
            _healthPoints = GetComponent<BaseStats>().GetStat(Stat.Health);
        }

        private void Update()
        {
            if (_isAlive && _healthPoints == 0)
            {
                Die();
            }
        }

        public void TakeDamage(GameObject instigator, float damage)
        {
            _healthPoints = Mathf.Max(_healthPoints - damage, 0);
            takeDamage.Invoke(damage);

            if (_isAlive && _healthPoints == 0)
            {
                onDie.Invoke();
                Die();
                AwardExperience(instigator);
            } 
        }        

        public float GetHealthPercentage()
        {
            return Mathf.Floor(_healthPoints / GetComponent<BaseStats>().GetStat(Stat.Health) * 100);
        }

        private void Die()
        {
            if (!_isAlive) return;

            GetComponent<Animator>().SetTrigger("Die");
            GetComponent<ActionScheduler>().CancelCurrentAction();            
            _isAlive = false;
        }

        private void AwardExperience(GameObject instigator)
        {
            Experience experience = instigator.GetComponent<Experience>();
            if (experience == null) return;

            experience.GainExperience(GetComponent<BaseStats>().GetStat(Stat.ExperienceReward));
        }

        public bool IsAlive()
        {
            return _isAlive;
        }

        public object CaptureState()
        {
            return _healthPoints;
        }

        public void RestoreState(object state)
        {
            _healthPoints = (float)state;
        }
    }
}