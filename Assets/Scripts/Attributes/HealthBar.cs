using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace RPG.Attributes
{
    public class HealthBar : MonoBehaviour
    {
        [SerializeField] Health healthComponent;
        [SerializeField] Image foreground = null;

        void Update()
        {
            float percentage = healthComponent.GetHealthPercentage();

            if (percentage == 0)
            {
                gameObject.SetActive(false);
            }

            foreground.transform.localScale = new Vector3(percentage / 100, foreground.transform.localScale.y, foreground.transform.localScale.z);
        }
    }
}
