﻿using RPG.Combat;
using RPG.Movement;
using UnityEngine;
using RPG.Core;
using RPG.Attributes;
using System;

namespace RPG.Control
{
    public class AIController : MonoBehaviour
    {
        [SerializeField] float _chaseDistance = 5f;
        [SerializeField] PatrolPath _patrolPath;
        [SerializeField] float _waypointTolerance = 1f;
        [SerializeField] private float _shoutDistance = 5f;

        private GameObject _player;
        private Health _health;
        private Fighter _fighter;
        private Vector3 _guardPosition;
        private float _timeSinceLastSawPlayer = Mathf.Infinity;
        private float _timeSinceReachedWaypoint = 0;
        private float _dwellingTime = 2.0f;
        private float _suspicionTime = 3.0f;
        private int _currentWaypointIndex = 0;
        private float _timeSinceAggrevated = Mathf.Infinity;
        private float _agroCooldownTime = 4f;

        private void Awake()
        {
            _player = GameObject.FindWithTag("Player");
            _health = GetComponent<Health>();
            _fighter = GetComponent<Fighter>();
        }

        private void Start()
        {
            _guardPosition = transform.position;
        }

        private void Update()
        {
            if (!_health.IsAlive()) return;

            //if (IsInAttackRangeOfPlayer() && _fighter.CanAttack(_player))
            if (IsAggrevated() && _fighter.CanAttack(_player))
            {
                // Attack Behaviour
                GetComponent<Fighter>().Attack(_player);
                _timeSinceLastSawPlayer = 0;
                AggrevateNearbyEnemies();
            }
            else if (_timeSinceLastSawPlayer < _suspicionTime)
            {
                // Suspicion Behaviour
                GetComponent<ActionScheduler>().CancelCurrentAction();
                _timeSinceLastSawPlayer += Time.deltaTime;
            }
            else
            {
                // Patrol/Guard Behaviour
                _timeSinceLastSawPlayer = Mathf.Infinity;
                Vector3 nextPosition = _guardPosition;

                if (_patrolPath != null)
                {
                    if (AtWaypoint())
                    {
                        if (_timeSinceReachedWaypoint > _dwellingTime)
                        {
                            CycleWaypoint();
                            _timeSinceReachedWaypoint = 0;
                        }
                        else
                        {
                            _timeSinceReachedWaypoint += Time.deltaTime;
                        }
                    }

                    nextPosition = GetCurrentWaypoint();
                }

                GetComponent<Mover>().StartMoveAction(nextPosition);
            }

            _timeSinceAggrevated += Time.deltaTime;
        }

        public void Aggrevate()
        {
            _timeSinceAggrevated = 0;
        }

        private void AggrevateNearbyEnemies()
        {
            RaycastHit[] hits = Physics.SphereCastAll(transform.position, _shoutDistance, Vector3.up, 0);

            foreach (RaycastHit hit in hits)
            {
                AIController ai = hit.collider.GetComponent<AIController>();
                if (ai == null) continue;

                ai.Aggrevate();
            }
        }

        private bool AtWaypoint()
        {
            float distanceToWaypoint = Vector3.Distance(transform.position, GetCurrentWaypoint());
            return distanceToWaypoint < _waypointTolerance;
        }

        private void CycleWaypoint()
        {
            _currentWaypointIndex = _patrolPath.GetNextWaypointIndex(_currentWaypointIndex);
        }

        private Vector3 GetCurrentWaypoint()
        {
            return _patrolPath.GetWaypoint(_currentWaypointIndex);
        }

        private bool IsInAttackRangeOfPlayer()
        {
            return Vector3.Distance(transform.position, _player.transform.position) <= _chaseDistance;
        }

        private bool IsAggrevated()
        {
            float distanceToPlayer = Vector3.Distance(_player.transform.position, transform.position);
            return distanceToPlayer < _chaseDistance || _timeSinceAggrevated < _agroCooldownTime;
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.cyan;
            Gizmos.DrawWireSphere(transform.position, _chaseDistance);
        }
    }
}
